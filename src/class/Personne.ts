import IAdresse from "../models/IAdresse";

export default abstract class Personne {
    abstract nom: string;
    abstract addresse: IAdresse;

    public abstract getNom(): string
}