import Client from './Client';
import Medicament from './Medicament';
import Vaccin from './Vaccin';
import Utils from './Utils';
import IAdresse from '../models/IAdresse';


export default class Main
{
    private clients: Client[] = []; // : Client[] indique que la propriete privée clients ne peut contenir que des objets de type Client
    private medicaments: Medicament[] = []; // pareil pour les medicaments
    private vaccin: Vaccin[] = [];

    ajouterClient(nom: string, credit: number, vaccined: boolean = false, addresse : IAdresse)
    {
        const nouveauClient: Client = new Client(nom, credit, vaccined, addresse);

        this.clients.push(nouveauClient);

        console.log(this.clients)
    }

    ajouterMedicaments(nom : string, prix: number, stock: number)
    {
        const nouveauMedicament: Medicament = new Medicament(nom,prix, stock);

        this.medicaments.push(nouveauMedicament);

        console.log(this.medicaments)
    }

    ajouterVaccin(nom : string, prix: number, stock: number, type : string)
    {
        const nouveauVaccin : Vaccin = new Vaccin (nom, prix, stock, type);

        this.vaccin.push(nouveauVaccin);

        console.log(this.vaccin);
        
    }

    rechercheClient(nom : string)
    {
        return this.clients.filter(client => client.getNom() === nom)[0] // this.clients represente l'objet client (private clients: Client[] = []) La méthode filter() crée et retourne un nouveau tableau contenant tous les éléments du tableau client est un iterator getNom est une methode disponible dans la class client qui obtient le nom du client === nom verifie le nom.
    }

    rechercheMedicament(nom : string)
    {
        return this.medicaments.filter(medicament => medicament.getNom() === nom)[0]
    }

    rechercheVaccin(nom : string)
    {
        return this.vaccin.filter(vaccins => vaccins.getNom() === nom )[0]
    }
    allCli (){
        return this.clients
    }
    
    clientVaccined()
    {
        return this.clients.filter(client => client.vaccined === true)
         // filtre du tableau clien en fonction du booléen
    }

    ruptureStockMed(){
        return this.medicaments.filter(medicament => medicament.getStock() === 0)

    }

    updateStock(){
       const med = this.medicaments.filter(medicament => medicament.getStock() === 0)
       med.forEach(element => {

        element.augmenterStock(10)

        console.log(element.getNom() , element.getStock() );

        console.log("liste medocs",med)

           
       });
    }

}