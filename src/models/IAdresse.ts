export default interface IAdresse
{
    numero: number;
    rue: string;
    codePostal: number;
    ville: string;
}