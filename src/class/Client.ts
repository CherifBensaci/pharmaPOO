import IAdresse from "../models/IAdresse";
import Medicament from "./Medicament";
import Personne from "./Personne";
import Vaccin from "./Vaccin";

export default class Client extends Personne
{
     addresse : IAdresse;
    nom: string;
     credit: number;
    vaccined : boolean = false // Définition variable booléen pour savoir si vacciné ou pas 

    constructor(nom: string, credit: number, vaccined:boolean = false, addresse: IAdresse)
    {
        super()
        this.addresse= addresse;
        this.nom = nom;
        this.credit = credit;
        this.vaccined = vaccined;//
    }

    augmenterCredit(credit: number): void  // : void indique que cette methode ne renvoie rien
    {
        this.credit += credit;
    }

    diminuerCredit(credit: number): void // : void indique que cette methode ne renvoie rien
    {
        this.credit -= credit;
    }

    getCredit(): number // : number indique que cette methode renvoie un nombre
    {
        return this.credit;
    }

    getNom(): string // : string indique que cette methode renvoie une chaine de caractères
    {
        return this.nom;
    }

    acheterMedicaments(medicament : Medicament)
    {
        this.diminuerCredit(this.getCredit() - medicament.getPrix()); // enlève le prix du médicament au crédit client. exemple ( théodore à 300 euros) - le prix du medicament (aspromal qui coute 5.6) 

       console.log(medicament.getStock());
       
       
        medicament.diminuerStock(0) // actualise le stock enlever des boites de médicaments aux stock initial qui de 200

        console.log(medicament.getStock());
       
       
        //console.log(medicament.getPrix());

        
    }

    acheterVaccins(vaccin : Vaccin)
    {
        this.diminuerCredit(this.getCredit() - vaccin.getPrix()); // enlève le prix du vaccin au crédit client. exemple ( théodore à 300 euros) - le prix du vaccin (anti grippe qui coute 25) 

        this.vaccined = true

        console.log(vaccin.getStock())

        vaccin.diminuerStock(1) // actualise le stock enleve 1 boites de vaccin aux stock initial qui de 500

        console.log(vaccin.getStock())

    }

    getAddresse()
    {
        return this.addresse;
    }

    
    
    
  

}