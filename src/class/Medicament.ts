export default class Medicament
{
    private nom : string;
    private prix : number;
    private stock :number;
    constructor(nom : string, prix : number, stock :number)
    {

        this.nom = nom;
        this.prix = prix;
        this.stock = stock;

    }

    augmenterStock(stock : number): void// : void indique que cette methode ne renvoie rien
    {
        this.stock += stock;
    }
    
    diminuerStock(stock : number): void 
    {
        this.stock -= stock;
    }

    getStock(): number
    {
        return this.stock;
    }

    getPrix(): number
    {
        return this.prix;
    }

    getNom(): string
    {
        return this.nom;
    }

}